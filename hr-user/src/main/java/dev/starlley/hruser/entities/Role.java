package dev.starlley.hruser.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_role")
public class Role  implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String role_name;
	
	public Role(){
		
	}	
	
		public Role(Long id, String role_name) {
		super();
		this.id = id;
		this.role_name = role_name;
	}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getRoleName() {
			return role_name;
		}

		public void setRoleName(String role_name) {
			this.role_name = role_name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((role_name == null) ? 0 : role_name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Role other = (Role) obj;
			if (role_name == null) {
				if (other.role_name != null)
					return false;
			} else if (!role_name.equals(other.role_name))
				return false;
			return true;
		}
		
		
	
	
}
