package dev.starlley.hrpayroll.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dev.starlley.hrpayroll.entities.Payment;
import dev.starlley.hrpayroll.entities.Worker;
import dev.starlley.hrpayroll.feignclients.WorkerFeignClient;

@Service
public class PaymentService {
	
	@Autowired
	private WorkerFeignClient wfc;

	public Payment getPayment(long workerId, int days) {
		
		/**
		 * Requisição externa para API com Feign
		 */
		Worker worker = wfc.findById(workerId).getBody();	
		
		return new Payment(worker.getName(), worker.getDailyIncome(), days);
	}
	
}
