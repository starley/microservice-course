package dev.starlley.hrworker.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.starlley.hrworker.entities.Worker;
import dev.starlley.hrworker.repositories.WorkerRepositorie;

@RefreshScope // Usado par alterações em runtime
@RestController
@RequestMapping(value = "/workers")
public class WorkerResource {

	@Value("${test.config}")
	private String testConfig;
	
	private static Logger logger = LoggerFactory.getLogger(WorkerResource.class);

	@Autowired
	private Environment env;

	@Autowired
	private WorkerRepositorie wr;

//	Pegando as configuracoes do repositorio git
	@GetMapping(value = "/configs")
	public ResponseEntity<Void> getConfigs() {

		logger.info("CONFIG = " + testConfig);

		return ResponseEntity.noContent().build();
	}

//	Listando todos os trabalhadores
	@GetMapping
	public ResponseEntity<List<Worker>> findAll() {
		List<Worker> list = wr.findAll();
		return ResponseEntity.ok(list);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Worker> findById(@PathVariable Long id) {

//		try {
//			Thread.sleep(3000L);
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}

		logger.info("PORT = " + env.getProperty("local.server.port"));

		Worker obj = wr.findById(id).get();
		return ResponseEntity.ok(obj);
	}

}
