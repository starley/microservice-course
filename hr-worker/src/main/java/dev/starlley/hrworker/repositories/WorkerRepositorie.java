package dev.starlley.hrworker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import dev.starlley.hrworker.entities.Worker;

public interface WorkerRepositorie extends JpaRepository<Worker, Long>{

}
